terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = ">=3.0.0"
    }
  }
}

provider "azurerm" {
  features {}
}

resource "azurerm_resource_group" "rg-nodes" {
  name     = "nodes"
  location = "eastus"
}

resource "azurerm_virtual_network" "vn-nodes" {
  name                = "nodes"
  address_space       = ["10.0.0.0/16"]
  location            = azurerm_resource_group.rg-nodes.location
  resource_group_name = azurerm_resource_group.rg-nodes.name
}

resource "azurerm_subnet" "s-node1" {
  name                 = "internal"
  resource_group_name  = azurerm_resource_group.rg-nodes.name
  virtual_network_name = azurerm_virtual_network.vn-nodes.name
  address_prefixes     = ["10.0.2.0/24"]
}

resource "azurerm_subnet" "s-node2" {
  name                 = "internal"
  resource_group_name  = azurerm_resource_group.rg-nodes.name
  virtual_network_name = azurerm_virtual_network.vn-nodes.name
  address_prefixes     = ["10.0.2.0/24"]
}

resource "azurerm_network_interface" "nic-node1" {
  name                = "nic-node1"
  location            = azurerm_resource_group.rg-nodes.location
  resource_group_name = azurerm_resource_group.rg-nodes.name

  ip_configuration {
    name                          = "internal"
    subnet_id                     = azurerm_subnet.s-node1.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = azurerm_public_ip.ip-node1.id
  }
}
resource "azurerm_public_ip" "ip-node1" {
  name                = "ip-node1"
  resource_group_name = azurerm_resource_group.rg-nodes.name
  location            = azurerm_resource_group.rg-nodes.location
  allocation_method   = "Static"

  tags = {
    environment = "Production"
  }
}

resource "azurerm_network_interface" "nic-node2" {
  name                = "example-nic"
  location            = azurerm_resource_group.rg-nodes.location
  resource_group_name = azurerm_resource_group.rg-nodes.name

  ip_configuration {
    name                          = "internal"
    subnet_id                     = azurerm_subnet.s-node2.id
    private_ip_address_allocation = "Dynamic"
  }
}



resource "azurerm_linux_virtual_machine" "vm-node1" {
  name                = "node1"
  resource_group_name = azurerm_resource_group.rg-nodes.name
  location            = azurerm_resource_group.rg-nodes.location
  size                = "Standard_B1s"
  admin_username      = "medberg"
  network_interface_ids = [
    azurerm_network_interface.nic-node1.id,
  ]

  admin_ssh_key {
    username   = "medberg"
    public_key = file("C:/Users/Simo/.ssh/id_rsa.pub")
  }

  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }
   source_image_reference {
    publisher = "Canonical"
    offer     = "0001-com-ubuntu-server-jammy"
    sku       = "22_04-lts-gen2"
    version   = "latest"
  }
}

data "azurerm_public_ip" "data-ip" {
  name                = azurerm_public_ip.ip-node1.name
  resource_group_name = azurerm_resource_group.rg-nodes.name
}

output "public_ip_address" {
  value = data.azurerm_public_ip.data-ip.ip_address
}

resource "azurerm_linux_virtual_machine" "vm-node2" {
  name                = "node2"
  resource_group_name = azurerm_resource_group.rg-nodes.name
  location            = azurerm_resource_group.rg-nodes.location
  size                = "Standard_B1s"
  admin_username      = "medberg"
  network_interface_ids = [
    azurerm_network_interface.nic-node2.id,
  ]

  admin_ssh_key {
    username   = "medberg"
    public_key = file("C:/Users/Simo/.ssh/id_rsa.pub")
  }

  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }

 
  source_image_reference {
    publisher = "Debian"
    offer     = "debian-10"
    sku       = "10"
    version   = "latest"
  }
}

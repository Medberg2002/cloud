terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = ">=3.0.0"
    }
  }
}

provider "azurerm" {
  features {}
}

resource "azurerm_resource_group" "minecraft" {
    name = "MedCraft"
    location = "West Europe"
}

resource "azurerm_storage_account" "minecraft" {
  name = "medcraft"
  resource_group_name = azurerm_resource_group.minecraft.name
  location = azurerm_resource_group.minecraft.location
  account_tier = "Standard"
  account_replication_type = "GRS"

  tags = {
  environment = "production"
  }
}

resource "azurerm_storage_share" "minecraft_mods" {
  name = "mods"
  storage_account_name = azurerm_storage_account.minecraft.name
  quota = 1
}

resource "azurerm_container_group" "minecraft" {
    name = "minecraft"
    location = azurerm_resource_group.minecraft.location
    resource_group_name = azurerm_resource_group.minecraft.name
    ip_address_type = "Public"
    dns_name_label = "Medcraft"
    os_type = "Linux"
    container {
      name = "server"
      image = "itzg/minecraft-server:java8-multiarch"
      cpu = "1"
      memory ="8"
      ports {
        port = 25565
        protocol = "TCP"
      }
      environment_variables = {
        VERSION = "1.12.2",
        TYPE="FORGE",
        MODS="https://mediafilez.forgecdn.net/files/2755/798/WR-CBE-1.12.2-2.3.2.33-universal.jar,https://mediafilez.forgecdn.net/files/2755/790/ForgeMultipart-1.12.2-2.6.2.83-universal.jar,https://mediafilez.forgecdn.net/files/2779/848/CodeChickenLib-1.12.2-3.2.3.358-universal.jar"
        JAVA_MEMORY="8G",
        MINECRAFT_MOTD="MedCraft for it4"
        MINECRAFT_RCON_ENABLED=true,
        MINECRAFT_RCON_PASSWORD="password",
        MINECRAFT_WHITELIST_ENABLED=false,
        EULA=true,
      }

    }
}
